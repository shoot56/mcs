$(document).ready(function(){
	initMap();
	$('.cote-slider-list').slick({
		dots: false,
		infinite: true,
		speed: 300,
		slidesToShow: 6,
		slidesToScroll: 6,
		responsive: [
			{
			  breakpoint: 1024,
			  settings: {
			    slidesToShow: 4,
			    slidesToScroll: 4
			  }
			},
			{
			  breakpoint: 600,
			  settings: {
			    slidesToShow: 2,
			    slidesToScroll: 2
			  }
			},
			{
			  breakpoint: 480,
			  settings: {
			    slidesToShow: 1,
			    slidesToScroll: 1
			  }
			}
		]
	});
	$('.partneer-slider-list').slick({
		dots: true,
		arrows: false,
		infinite: true,
		speed: 300,
		slidesToShow: 4,
		slidesToScroll: 4,
		responsive: [
			{
			  breakpoint: 1024,
			  settings: {
			    slidesToShow: 3,
			    slidesToScroll: 3
			  }
			},
			{
			  breakpoint: 600,
			  settings: {
			    slidesToShow: 3,
			    slidesToScroll: 3
			  }
			},
			{
			  breakpoint: 499,
			  settings: {
			    slidesToShow: 1,
			    slidesToScroll: 1
			  }
			}
		]
	});
	$('.tradingSlider-list').slick({
		dots: true,
		arrows: false,
		infinite: true,
		speed: 300,
		slidesToShow: 4,
		slidesToScroll: 4,
		responsive: [
			{
			  breakpoint: 1024,
			  settings: {
			    slidesToShow: 3,
			    slidesToScroll: 3
			  }
			},
			{
			  breakpoint: 600,
			  settings: {
			    slidesToShow: 2,
			    slidesToScroll: 2
			  }
			},
			{
			  breakpoint: 499,
			  settings: {
			    slidesToShow: 1,
			    slidesToScroll: 1
			  }
			}
		]
	});
	$('.license-slider-list').slick({
		dots: true,
		arrows: false,
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1,
	});
	$('.bg-img').each(function(){
		$(this).closest('.bg-img-wrap').css('backgroundImage','url('+$(this).attr('src')+')');
	});
	$('.drop-open').click(function(e) {
		var item = $(this).closest('li');
		item.toggleClass('drop-active');
		item.find('.drop').slideToggle(400);
		e.preventDefault();
	});
	$(".nav-opener").click(function(e) {
		e.preventDefault();
		$('body').addClass('nav-active');
	});
	$(".mobile-nav-closer").click(function(e) {
		e.preventDefault();
		$('body').removeClass('nav-active');
	});
	$('.tooltip').tooltipster({
		theme: 'tooltipster-noir',
		maxWidth: 300,
		arrow: false
	});
	$(".select-holder select").dropdown({
		
	});
	$(".checkbox-holder input[type=checkbox]").checkbox({
		
	});
	// popups
	$('body').popup({
		"opener":".login-popup-opener",
		"popup_holder":"#login-popup",
		"popup":".popup",
		"close_btn":".close"
	}).popup({
		"opener":".open-account-popup-opener",
		"popup_holder":"#open-account-popup",
		"popup":".popup",
		"close_btn":".close"
	});
	// accordion
	$('.accordion .item .opener-icon').click(function(e){
		var item=$(this).closest('.item');
		if(item.hasClass('active')){
			item.find('.expanded').slideToggle(function(){
				item.toggleClass('active');
			});
		}else{
			item.parent().find('.item.active .expanded').slideToggle(function(){
				$(this).parent().toggleClass('active');
			});
			item.find('.expanded').slideToggle(function(){
				item.toggleClass('active');
			});
		}
		e.preventDefault();
	});




});

// function parallaxScroll(){
//     var scrolled = $(window).scrollTop();
// 	$('.main-promo').css('background-position', '50% '+ ((scrolled*.1))+'px');
// };



function initMap() {
	if(!$('#map-canvas').size())return;
	function initialize() {
	  var mapOptions = {
	    zoom: 17,
		scrollwheel: false,
	    navigationControl: false,
	    mapTypeControl: false,
	    scaleControl: false,
	    draggable: false,
	    center: new google.maps.LatLng(55.791867, 37.706798)
	  }
	  var map = new google.maps.Map(document.getElementById('map-canvas'),
	                                mapOptions);

	  var image = 'images/map-ico.png';
	  var myLatLng = new google.maps.LatLng(55.791771, 37.705875);
	  var beachMarker = new google.maps.Marker({
	      position: myLatLng,
	      map: map,
	      icon: image
	  });
	}
	google.maps.event.addDomListener(window, 'load', initialize);
}



$.fn.popup = function(o){
	var o = $.extend({
		"opener":".call-back a",
		"popup_holder":"#call-popup",
		"popup":".popup",
		"close_btn":".btn-close",
		"close":function(){},
		"beforeOpen": function(popup) {
			$(popup).css({
				'left': 0,
				'top': 0
			}).hide();
		}
	},o);
	return this.each(function(){
		var container=$(this),
			opener=$(o.opener,container),
			popup_holder=$(o.popup_holder,container),
			popup=$(o.popup,popup_holder),
			close=$(o.close_btn,popup),
			bg=$('.bg',popup_holder);
			popup.css('margin',0);
			opener.click(function(e){
				o.beforeOpen.apply(this,[popup_holder]);
				popup_holder.fadeIn(400);
				alignPopup();
				bgResize();
				e.preventDefault();
			});
		function alignPopup(){
			var deviceAgent = navigator.userAgent.toLowerCase();
			var agentID = deviceAgent.match(/(iphone|ipod|ipad|android)/i);
			if(agentID){
				if(popup.outerHeight()>window.innerHeight){
					popup.css({'top':$(window).scrollTop(),'left': ((window.innerWidth - popup.outerWidth())/2) + $(window).scrollLeft()});
					return false;
				}
				popup.css({
					'top': ((window.innerHeight-popup.outerHeight())/2) + $(window).scrollTop(),
					'left': ((window.innerWidth - popup.outerWidth())/2) + $(window).scrollLeft()
				});
			}else{
				if(popup.outerHeight()>$(window).outerHeight()){
					popup.css({'top':$(window).scrollTop(),'left': (($(window).width() - popup.outerWidth())/2) + $(window).scrollLeft()});
					return false;
				}
				popup.css({
					'top': (($(window).height()-popup.outerHeight())/2) + $(window).scrollTop(),
					'left': (($(window).width() - popup.outerWidth())/2) + $(window).scrollLeft()
				});
			}
		}
		function bgResize(){
			var _w=$(window).width(),
				_h=$(document).height();
			bg.css({"height":_h,"width":_w+$(window).scrollLeft()});
		}
		$(window).resize(function(){
			if(popup_holder.is(":visible")){
				bgResize();
				alignPopup();
			}
		});
		if(popup_holder.is(":visible")){
				bgResize();
				alignPopup();
		}
		close.add(bg).click(function(e){
			var closeEl=this;
			popup_holder.fadeOut(400,function(){
				o.close.apply(closeEl,[popup_holder]);
				$('.video-popup iframe').attr('src','');
			});
			e.preventDefault();
		});
		$('body').keydown(function(e){
			if(e.keyCode=='27'){
				popup_holder.fadeOut(400);
			}
		})
	});
};